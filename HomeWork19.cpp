﻿#include <iostream>

class IAnimal {
public:
    virtual void Voice() = 0;

};

class Mouse : public IAnimal {
public:
    void Voice() override {
        std::cout << "pi-pi-pi"  << std::endl;
    }
};

class Cat : public IAnimal {
public:
    void Voice() override {
        std::cout << "muurrr-murrrr" << std::endl;
    }
};

class Dog : public IAnimal {
public:
    void Voice() {
        std::cout << "gaaav-gaaav" << std::endl;
    }
};

class Human : public IAnimal {
public:
    void Voice() override {
        std::cout << "Hello World!" << std::endl;
    }
};


int main()
{
    IAnimal* animals[] = {new Dog, new Human, new Cat, new Mouse};
    for (auto animal : animals) {
        animal->Voice();
    }
    return 0;
}
